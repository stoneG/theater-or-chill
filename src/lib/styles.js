import { css } from 'emotion'

export function createStyles(styles) {
  for (let prop in styles) {
    styles[prop] = css(styles[prop])
  }
  return styles
}