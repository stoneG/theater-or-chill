import axios from 'axios'
import config from 'config'

class TMDB {
  constructor() {
    this.root = config.get('tmdb.api.root')
    this.key = config.get('tmdb.api.key')
  }

  get(endpoint, params) {
    return axios.get(`${this.root}${endpoint}`, {
      params: {
        api_key: this.key,
        ...params,
      },
    })
  }

  async getPopularMovies() {
    const response = await this.get('/movie/popular')
    return response.data.results
  }

  async getMovies(search) {
    const response = await this.get('/search/movie', { query: search })
    return response.data.results
  }

  async getMovie(id) {
    const response = await this.get(`/movie/${id}`)
    return response.data
  }
}

const tmdb = new TMDB()

export default tmdb