import express from 'express'
import api from './api'

const PORT = 4000

const app = express()

app.use('/api', api)

app.get('/', (req, res) => {
  res.send('Hello World!')
})

app.listen(PORT, () =>
  console.log(`Listening on port ${PORT}`)
)