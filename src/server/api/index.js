import config from 'config'
import { Router } from 'express'
import tmdb from '../../lib/tmdb'

const api = Router()

const throwOnAsyncError = fn =>
  (req, res, next) => {
    Promise
      .resolve(fn(req, res, next))
      .catch(next)
  }

api.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next()
})

api.get('/', (req, res) => {
  res.send(`Theater or Chill API.\nTMDB API Key: ${config.get('tmdb.api.key')}`)
})

api.get('/movies/popular', throwOnAsyncError(async (req, res) => {
  const movies = await tmdb.getPopularMovies()
  res.send(JSON.stringify(movies))
}))

api.get('/movies', throwOnAsyncError(async (req, res) => {
  const movies = await tmdb.getMovies(req.query.search)
  res.send(JSON.stringify(movies))
}))

api.get('/movie/:id', throwOnAsyncError(async (req, res) => {
  const movie = await tmdb.getMovie(req.params.id)
  res.send(JSON.stringify(movie))
}))

export default api