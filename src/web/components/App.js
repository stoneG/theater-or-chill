import axios from 'axios'
import _ from 'lodash'
import React, { Component } from 'react'
import Dialog from '@material-ui/core/Dialog'

import { createStyles } from '../../lib/styles'
import logo from '../../images/logo.png'
import tmdb from '../../images/tmdb.svg'

class App extends Component {
  constructor(props) {
    super(props)

    this.state = {
      search: '',
      popularMovies: [],
      matchingMovies: [],
    }
  }

  componentDidMount() {
    this.init()
  }

  async init() {
    const response = await axios.get('http://localhost:4000/api/movies/popular')
    this.setState({ popularMovies: response.data })
  }

  renderMovie = movie => {
    const { id, title, release_date, poster_path } = movie
    return (
      <div className={styles.movie} onClick={this.selectMovie.bind(this, movie)}>
        <img
          src={`https://image.tmdb.org/t/p/w300_and_h450_bestv2/${poster_path}`}
          alt={title}
         />
        <div className={styles.movieInfo}>
          <p>{title}</p>
          <p className={styles.releaseDate}>
            {(new Date(release_date)).getFullYear()}
          </p>
        </div>
      </div>
    )
  }

  renderMovieDetail = movieDetail => {
    const {
      title,
      genres,
      poster_path,
      release_date
    } = movieDetail

    const genresStr = genres ? genres.map(g => g.name).join(', ') : ''

    return (
      <div className={styles.modalContainer}>
        <img src={`https://image.tmdb.org/t/p/w300_and_h450_bestv2/${poster_path}`} />
        <ul className={styles.metadata}>
          <li className={styles.metadataTitle}>{title}</li>
          <li>{(new Date(release_date)).getFullYear()}</li>
          <li>{genresStr}</li>
        </ul>
      </div>
    )
  }

  render() {
    const {
      search,
      matchingMovies,
      popularMovies,
      movieDetail,
      searching
    } = this.state

    return (
      <div className={styles.container}>

        <img src={logo} className={styles.logo} />

        <input
          value={search}
          onChange={this.onChangeMovieSearch}
          className={styles.searchInput}
          placeholder="Search for a movie"
        />

        <div className={styles.movies}>
          {!!search.length && !searching && matchingMovies.map(this.renderMovie)}
          {!search.length && popularMovies.map(this.renderMovie)}
        </div>

        {!searching && !matchingMovies.length &&
          <h2 className={styles.empty}>
            We could not find any movies matching "{search}"
          </h2>
        }

        <img src={tmdb} className={styles.tmdb} />

        <Dialog
          open={!!movieDetail}
          onClose={this.closeMovieDetail}
        >
          {!!movieDetail && this.renderMovieDetail(movieDetail)}
        </Dialog>

      </div>
    )
  }

  onChangeMovieSearch = (e) => {
    const { value } = e.target
    if (value) {
      this.searchMovie(value)
      this.setState({ search: value, searching: true })
    } else {
      this.setState({ search: value, searching: false, matchingMovies: [] })
    }
  }

  searchMovie = _.debounce(async search => {
    const response = await axios.get('http://localhost:4000/api/movies', { params: { search } })
    if (this.state.search === search) {
      this.setState({
        matchingMovies: response.data,
        searching: false,
      })
    }
  }, 300)

  selectMovie = (movie) => {
    this.setState({ movieDetail: movie })
    this.updateMovieDetail(movie.id)
  }

  updateMovieDetail = async (id) => {
    const response = await axios.get(`http://localhost:4000/api/movie/${id}`)
    const { movieDetail } = this.state
    if (movieDetail.id === response.data.id) {
      this.setState({ movieDetail: { ...this.state.movieDetail, ...response.data } })
    }
  }

  closeMovieDetail = () => {
    this.setState({ movieDetail: undefined })
  }
}

export default App

const styles = createStyles({
  container: {
    position: 'relative',
    margin: '0 auto',
    display: 'flex',
    width: '90%',
    paddingBottom: '4em',
    flexDirection: 'column',
    alignItems: 'center',
    color: '#eee',
  },

  logo: {
    marginTop: '10em',
    maxWidth: 1111,
  },

  searchInput: {
    maxWidth: 500,
    margin: '1em 0 2em',
    width: '80%',
    padding: '0.125em 0.5em 0.25em',
    fontSize: '2em',
    backgroundColor: 'rgba(255, 255, 255, 0.2)',
    border: '1px solid transparent',
    borderRadius: 3,
    color: '#ccc',
    transition: '0.2s ease all',
    ':hover': {
      borderColor: 'rgba(255, 255, 255, 0.1)',
    },
  },

  movies: {
    display: 'grid',
    maxWidth: '100%',
    gridTemplateColumns: '1fr',
    gridColumnGap: '2em',
    gridRowGap: '2em',
    justifyItems: 'center',
    '@media (min-width: 800px)': {
      gridTemplateColumns: '1fr 1fr',
    },
    '@media (min-width: 1000px)': {
      gridTemplateColumns: '1fr 1fr 1fr',
    }
  },

  movie: {
    maxWidth: 300,
    color: '#eee',
    opacity: 0.8,
    cursor: 'pointer',
    transition: '0.2s ease all',
    ':hover': {
      opacity: 1,
    }
  },

  movieInfo: {
    display: 'flex',
    alignItems: 'flex-start',
    justifyContent: 'space-between',
    padding: '0.25em',
  },

  modalContainer: {
    padding: '2em',
    width: 300,
    backgroundColor: '#444',
  },

  metadata: {
    margin: 0,
    padding: 0,
    listStyle: 'none',
    color: '#eee',
    lineHeight: 1.4,
  },

  metadataTitle: {
    marginTop: '0.25em',
    marginBottom: '0.5em',
    fontSize: '1.5em',
    lineHeight: 1.1,
  },

  releaseDate: {
    color: '#aaa'
  },

  empty: {
    textAlign: 'center',
  },

  tmdb: {
    marginTop: '3em',
    width: 100,
  },
})