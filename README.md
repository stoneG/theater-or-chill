# Theater Or Chill

## Setup
* Clone the repo.
* Make a copy of `/config/default.js`, call it `local.js`. Add your TMDB api key into the `local.js` copy.
* `npm i`

## Running the app
Run the following commands in separate terminal instances:
```
# start api server
npm run server

# start react app
npm start
```

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

API Powered by THE MOVIE DB.
